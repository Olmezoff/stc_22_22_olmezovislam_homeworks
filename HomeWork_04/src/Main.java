public class Main {
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++){
            sum += a[i];
        }
        return sum;
    }
    public static boolean isEven (int number){
        return number % 2 == 0;
    }
    public static void main(String[] args) {
        int[] array = {3, 15, 16, 23, 42, 32, 67, 91, 7, 12};
        int sum1 = calcSumOfArrayRange(array, 3, 7);
        System.out.println(sum1);
        if (isEven(sum1)){
            System.out.println("Число четное");
        } else {
            System.out.println("Число не четное");
        }
    }
}