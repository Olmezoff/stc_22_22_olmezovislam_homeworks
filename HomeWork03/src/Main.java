import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int length = scan.nextInt();
        int[] array = new int[length];

        for (int i = 0; i < array.length; i ++) {
            array[i] = scan.nextInt();
            System.out.println(array[i]);
        }

    }
}